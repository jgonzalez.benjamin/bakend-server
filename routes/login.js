var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

var SEED = require('../config/config').SEED;

var app = express();

var Usuario = require('../models/usuario');

const CLIENT_ID = require('../config/config').CLIENT_ID;
const SECRET_ID = require('../config/config').SECRET_ID;

var {OAuth2Client} = require('google-auth-library');

var mdAutenticacion = require('../middlewares/autenticacion');


// ================================================================
//  Actualiza token
// ================================================================
app.get('/renuevatoken', mdAutenticacion.verificaToken, (req,res) => {
    var token = jwt.sign({usuario: req.usuario}, SEED,{ expiresIn: 14400 });

    res.status(200).json({
        ok: true,
        token: token
    })
});


// ================================================================
//  Autnenticación de  Sr. Google
// ================================================================
app.post('/google', (req, res) =>{

    var token =  req.body.token || 'XXX';
    
    var client = new OAuth2Client(CLIENT_ID);

    async function verify() {
        var ticket = await client.verifyIdToken({
            idToken: token,
            audience: CLIENT_ID,  // Specify the CLIENT_ID of the app that accesses the backend
            // Or, if multiple clients access the backend:
            //[CLIENT_ID_1, CLIENT_ID_2, CLIENT_ID_3]
            function (e, login) {
                if (e) {
                    return res.status(400).json({
                        ok:false,
                        mensaje: 'Token invalido',
                        errors: e.message
                    });
                }         
            }
        });

        var payload = ticket.getPayload();
        var userid = payload['sub'];
        // If request specified a G Suite domain:
        //const domain = payload['hd'];


        Usuario.findOne({email: payload.email}, (err, usuario)=>{
            if (err) {
                return res.status(500).json({
                    ok:false,
                    mensaje: 'Error al buscar usuario-Login',
                    errors: err.message
                });
            }
            if (usuario) {
                if (!usuario.google) {
                    return res.status(500).json({
                        ok:false,
                        mensaje: 'Debe usar su autenticaión normal',
                        errors: err.message
                    });
                }else{
                    var token = jwt.sign({usuario: usuario}, SEED,{ expiresIn: 14400 });
                    /** 14400 son 4 hs */
                    usuario.password = ':)';

                    res.status(200).json({
                        ok:true,
                        usuario: usuario,
                        id: usuario.id,
                        token: token,
                        menu: obtenerMenu( usuario.role )
                    });
                }
            }else{/* Si el usuario no existe por correo */
                var usuario = new Usuario();

                usuario.nombre =  payload.name;
                usuario.email = payload.email;
                usuario.img = payload.picture;
                usuario.password = ':)';
                usuario.google = true;

                usuario.save((err, usuarioDB)=>{
                    if (err) {
                        return res.status(400).json({
                            ok:false,
                            mensaje: 'Error al crear usuario',
                            errors: err.message
                        });
                    }

                    var token = jwt.sign({usuario: usuarioDB}, SEED,{ expiresIn: 14400 });
                    /** 14400 son 4 hs */
                    usuarioDB.password = ':)';

                    res.status(200).json({
                        ok:true,
                        usuario: usuarioDB,
                        id: usuarioDB.id,
                        token: token,
                        menu: obtenerMenu( usuarioDB.role )
                    }); 

                }); 
            }
        });

    }
    verify().catch(console.error); 

});



// ================================================================
//  Autnticación normal
// ================================================================

app.post('/',(req, res) => {
    var body = req.body;

    Usuario.findOne({email: body.email}, (err, usuarioDB)=>{

        if (err) {
            return res.status(500).json({
                ok:true,
                errors: err.message
            });
        }
        if ( !usuarioDB ) {
            return res.status(400).json({
                ok:false,
                errors: err,
                mensaje: 'Credenciales incorrectas - email'
                /** mas a deante sacar el '- email' cuando valla a produccion*/
            });
        }

        if ( !bcrypt.compareSync(body.password, usuarioDB.password) ) {
            return res.status(500).json({
                ok:false,
                errors: err,
                mensaje: 'Credenciales incorrectas - password'
                /** mas adelante hay que sacar el '- password' cuando valla a produccion */
            });
        }
        // ================================================================
        //  Crea un token
        // ================================================================
        var token = jwt.sign({usuario: usuarioDB}, SEED,{ expiresIn: 14400 });
                                                        /** 14400 son 4 hs */
        usuarioDB.password = ':)';
        
        res.status(200).json({
            ok:true,
            usuario: usuarioDB,
            id: usuarioDB.id,
            token: token,
            menu: obtenerMenu( usuarioDB.role )
        });

    });


});

function obtenerMenu( role ){
   var menu = [
        {
          titulo: 'Principal',
          icono: 'mdi mdi-gauge',
          submenu: [
            { titulo: 'Dashboard', url: '/dashboard' },
            { titulo: 'ProgessBar', url: '/progress' },
            { titulo: 'Gráficas', url: '/graphics1' },
            { titulo: 'Promesas', url: '/promesas' },
            { titulo: 'Rxjs', url: '/rxjs' }
          ]
        },
        {
          titulo: 'Mantenimiento',
          icono: 'mdi mdi-folder-lock-open',
          submenu: [
            /* { titulo: 'Usuarios', url: '/usuarios' }, */
            { titulo: 'Hospitales', url: '/hospitales' },
            { titulo: 'Medicos', url: '/medicos' }
          ]
        }
      ];

      if(role === 'ADMIN_ROLE'){
          /* push: o pones al final y unshift: lo pone al principio */
          menu[1].submenu.unshift( { titulo: 'Usuarios', url: '/usuarios' } );
      }



    return menu;
}



module.exports = app;