var express = require('express');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Hospital = require('../models/hospital');

// ================================================================
// 'GET'
// ================================================================
app.get('', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Hospital.find({})
    .skip(desde).populate('usuario', 'nombre email role')
        .exec((err, hospitales) => {
            if ( err ) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'error loading hospitals',
                    errors: err
                });
            }
        
            Hospital.count({}, (err, conteo)=>{

                if (err) {
                    return res.status(400).json({ 
                        ok:false,
                        errors: err.message
                    });
                }

                res.status(200).json({
                    ok: true,
                    total: conteo,
                    hospitales: hospitales
                });

            });
        });

});

// ================================================================
// GET hospital espescifico 
// ================================================================
app.get('/:id', (req, res) => {
    var id = req.params.id;
        Hospital.findById(id).populate('usuario', 'nombre img email').exec((err, hospital) => {
        if (err) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar hospital',
                errors: err
            });
        }
        if (!hospital) {
            return res.status(400).json({
                ok: false,
                mensaje: 'El hospital con el id ' + id + ' no existe',
                errors: { message: 'No existe un hospital con ese ID' }
            });
        }
        res.status(200).json({
            ok: true,
            hospital: hospital
        });
    });
});

// ================================================================
// 'PUT'
// ================================================================
app.put('/:id', mdAutenticacion.verificaToken, (req, res) => { 
/** poneiendo el ':id' estamos haciendo obligatorio el envio del id en la app */
    var id = req.params.id;

    Hospital.findById( id, (err, hospital) =>{

        if ( err ) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error not found',
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        if (!hospital) {
            return res.status(404).json({
                ok: false,
                mensaje: 'Hospital id: '+id+' not found',
                errors: err.message/** gracias al psotman pude hacer esto */
            });
            
        }

        var body = req.body;

        hospital.nombre = body.nombre;
        hospital.usuario = req.usuario._id;

        hospital.save( (err, hospitalGuardado) =>{
            if ( err ) {
                return res.status(400).json({
                    ok: false,
                    errors: err.message/** gracias al psotman pude hacer esto */
                });
            }

            res.status(200).json({
                ok: true,
                hospital: hospitalGuardado
            });

        });

    });


});

// ================================================================
// 'POST'
// ================================================================
app.post('/', mdAutenticacion.verificaToken , (req, res) =>{
    var body = req.body;/** esto solo funciona si y solo si se tiene el body-parser */

    var hospital = new Hospital({
        nombre: body.nombre,
        img: body.img,
        usuario: req.usuario._id,
    });

    hospital.save( (err, hospitalGuardado) =>{
        if ( err ) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error creating hospital',
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        res.status(201).json({
            ok: true,
            hospital: hospitalGuardado
        });
    });

});

// ================================================================
// 'DELETE'
// ================================================================
app.delete('/:id', mdAutenticacion.verificaToken ,(req, res) =>{
    var id = req.params.id;

    Hospital.findByIdAndRemove(id, (err, hospitalBorrado) =>{
        if ( err ) {
            return res.status(500).json({
                ok: false,
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        if ( !hospitalBorrado ) {
            return res.status(404).json({
                ok: false,
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        res.status(200).json({
            ok: true,
            hospital: hospitalBorrado
        });
    });
});



module.exports = app;
