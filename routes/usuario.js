var express = require('express');
var bcrypt = require('bcryptjs');
var jwt = require('jsonwebtoken');

var mdAutenticacion = require('../middlewares/autenticacion');

// var SEED = require('../config/config').SEED; /** se usa para la funcion commentada */

var app = express();

var Usuario = require('../models/usuario');

// ================================================================
//  Obtener todos los usuarios 'GET'
// ================================================================
app.get('', (req, res, next) => {
    var desde = req.query.desde || 0;
    desde = Number(desde);

    Usuario.find({}, 'nombre email google role img')
    .skip(desde).limit(5).exec( (err, usuarios) => {
        if ( err ) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error cargando usuarios',
                errors: err
            });
        }

        Usuario.count({}, (err, conteo)=>{

            if (err) {
                return res.status(400).json({ 
                    ok:false,
                    errors: err.message
                });
            }

            res.status(200).json({
                ok: true,
                total: conteo,
                usuarios: usuarios
            });

        });
        
    });

});
// ================================================================
//  Verificar token   'MIDDLEWERE' esta forma es para blokear las 
//      peticiones que esten debajo
// ================================================================
// app.use('/',(req, res, next) => {
//     /** resibe el toke por la url */
//     var token = req.query.token;

//     jwt.verify( token, SEED, (err, decoded) => {
//         if (err) {
//             return res.status(401).json({
//                 ok:false,
//                 errors: err.message,
//                 mensaje: 'Token invalido'
//             });
//         }
//         next();
//     });

// });


// ================================================================
//  Actualizar usuario 'PUT'
// ================================================================
app.put('/:id', [mdAutenticacion.verificaToken, mdAutenticacion.verificaADMIN_o_MismoUsuario], (req, res) => { 
/** poniendo el ':id' estamos haciendo obligatorio el envio del id en la app */
    var id = req.params.id;

    Usuario.findById( id, (err, usuario) =>{

        if ( err ) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error al buscar usuario',
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        if (!usuario) {
            return res.status(404).json({
                ok: false,
                mensaje: 'Usuario id: '+id+' no encontrado',
                errors: err.message/** gracias al psotman pude hacer esto */
            });
            
        }

        var body = req.body;

        usuario.nombre = body.nombre;
        usuario.email = body.email;
        usuario.role = body.role;

        usuario.save( (err, usuarioGuardado) =>{
            if ( err ) {
                return res.status(400).json({
                    ok: false,
                    errors: err.message/** gracias al psotman pude hacer esto */
                });
            }
            /**otra forma de no mostrar la contraseña */
            usuarioGuardado.password = ':v';

            res.status(200).json({
                ok: true,
                usuario: usuarioGuardado
            });

        });

    });


});

// ================================================================
//  Crear un nuevo usuario 'POST'
// ================================================================
app.post('/', (req, res) =>{
    var body = req.body;/** esto solo funciona si y solo si se tiene el body-parser */

    var usuario = new Usuario({
        nombre: body.nombre,
        email: body.email,
        password: bcrypt.hashSync( body.password, 10),/** con esto se encripta la contraseña */
        img: body.img,
        role: body.role,
    });

    usuario.save( (err, usuarioGuardado) =>{
        if ( err ) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error al crear usuario',
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        res.status(201).json({
            ok: true,
            usuario: usuarioGuardado
        });
    });

});

// ================================================================
//  Eliminar un usuario mediente id 'DELETE'
// ================================================================
app.delete('/:id', [mdAutenticacion.verificaToken, mdAutenticacion.verificaADMIN_ROLE] ,(req, res) =>{
    var id = req.params.id;

    Usuario.findByIdAndRemove(id, (err, usuarioBorrado) =>{
        if ( err ) {
            return res.status(500).json({
                ok: false,
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        if ( !usuarioBorrado ) {
            return res.status(404).json({
                ok: false,
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        res.status(200).json({
            ok: true,
            usuario: usuarioBorrado
        });
    });
});



module.exports = app;
