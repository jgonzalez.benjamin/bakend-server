var Promise = require('es6-promise').Promise;

var express = require('express');

var Hospital = require('../models/hospital');
var Medico = require('../models/medico');
var Usuario = require('../models/usuario');

var app = express();

// ================================================================
//  Busqueda específica
// ================================================================
app.get('/coleccion/:tabla/:busqueda', (req, res)=>{
    var tabla = req.params.tabla;
    var busqueda = req.params.busqueda;
    var expReg = new RegExp(busqueda, 'i');

    var promesa;

    switch (tabla) {
        case 'hospitales':
           promesa = buscarHospitales(busqueda, expReg);   
            break;
        case 'medicos':
            promesa = buscarMedicos(busqueda, expReg);
            break;
         case 'usuarios':
            promesa = buscarUsuarios(busqueda, expReg);
            break;
        default:
            return res.status(404).json({
                ok: false,
                mensaje:'Error, busqueda solo con usuarios, medicos y hospitales',
                errors:{message: 'tipo de tabla/collecion inválido'}
            });
            break;
    }
    promesa.then(data=>{
        res.status(200).json({
            ok: true,
            [tabla]: data
            /** Las [] son para decirle a ES que ponga el resultado de la variable */
        });
    });

});


// ================================================================
//  Busqueda general
// ================================================================
app.get('/todo/:busqueda', (req, res, next) => {
    var busqueda = req.params.busqueda;
    var expReg = new RegExp(busqueda, 'i');

    Promise.all([ 
        buscarHospitales(busqueda, expReg), 
        buscarMedicos(busqueda, expReg), 
        buscarUsuarios(busqueda, expReg) 
    ])
    .then(respuestas=>{
        res.status(200).json({
            ok: true,
            hospitales: respuestas[0],
            medicos: respuestas[1],
            usuarios: respuestas[2]
        });
    })
});

// ================================================================
//  Creacion de promesas:
// ================================================================
function buscarHospitales(busqueda, expReg) {
    return new Promise(( resolve, reject )=>{
        Hospital.find({nombre: expReg})
        .populate('usuario', 'nombre img email').exec((err, hospitales)=>{
            if ( err ) {
                reject(err.message);
            } else {
                resolve(hospitales);
            }
        });
    });
}

function buscarMedicos(busqueda, expReg) {
    return new Promise(( resolve, reject )=>{
        Medico.find({nombre: expReg})
        .populate('usuario', 'nombre img email').populate('hospital','nombre')
        .exec(    (err, medicos)=>{
            if ( err ) {
                reject(err.message);
            } else {
                resolve(medicos);
            }
        });
    });
}

function buscarUsuarios(busqueda, expReg) {
    return new Promise(( resolve, reject )=>{
        Usuario.find({}, 'nombre img email role').or([{'nombre': expReg},{'email': expReg}])
        .exec(
            (err, usuarios)=>{
                if (err) {
                    reject(err.message);
                } else {
                    resolve(usuarios);
                }
            }
        );
    });
}
module.exports = app;