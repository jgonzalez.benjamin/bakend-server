var express = require('express');

var fileUpload = require('express-fileupload');

var Usuario = require('../models/usuario');
var Medico = require('../models/medico');
var Hospital = require('../models/hospital');

var fs = require('fs');

var app = express();
// default options
app.use(fileUpload());

// ================================================================
//  Upload de imagen por PUT
// ================================================================

app.put('/:tipo/:id', (req, res, next) => {

    var tipo = req.params.tipo;
    var id = req.params.id;

    /* Tipos de collecion */
    var tiposValidos = ['hospitales', 'medicos', 'usuarios'];

    if (tiposValidos.indexOf( tipo ) < 0) {
        return res.status(400).json({
            ok:false,
            errors: {message:'colecciones aceptadas: hospitales, medicos, usuarios '},
            mensaje: 'Tipo de coleccion invalida'
        });
    }

    if ( !req.files ) {
        return res.status(400).json({
            ok:false,
            errors: {message:'Debe de seleccionar una imagen!'},
            mensaje: 'No selecciono nada!'
        });
    }


    /* Obtener el nombre del arachivo */

    var arachivo = req.files.imagen;
    var nombreCortado = arachivo.name.split('.');
    var extencionArchivo = nombreCortado[ nombreCortado.length - 1 ];

    /* Nombre de archivo personalizado */
    var nombreArchivo =  `${ id }-${ new Date().getMilliseconds()  }.${ extencionArchivo }`;

    /* Mover el archivo a un path especifico */
    var path = `./uploads/${ tipo }/${ nombreArchivo }`;

    arachivo.mv( path , err => {

        if (err) {
            return res.status(500).json({
                ok:false,
                mensaje: 'Error al mover el archivo',
                errors: err.message
            });
        }

        subirPorTipo( tipo, id, nombreArchivo, res );

        /* res.status(200).json({
            ok:true,
            mensaje: 'Archivo movido',
            extencionArchivo: extencionArchivo
        }); */

    });


});

function subirPorTipo( tipo, id, nombreArchivo, res ) {
    if ( tipo === 'usuarios') {
        
        Usuario.findById(id, (err, usuario) => {

            var pathViejo = './uploads/usuarios/' + usuario.img;
            /* Si existe, elimina la imagen anterior */
            if (fs.existsSync( pathViejo)) {
                fs.unlink( pathViejo );
            }

            usuario.img = nombreArchivo;

            usuario.save((err, usuarioActualizado) => {
                return res.status(200).json({
                    ok:true,
                    mensaje: 'Imagen actualizada',
                    usuario: usuarioActualizado
                });    
            });
        });
    }
    if ( tipo === 'medicos') {
        Medico.findById(id, (err, medico) => {

            /* var pathViejo = './uploads/medicos/' + medico.img; */
            /* Si existe, elimina la imagen anterior */

            /* fs.unlink( pathViejo ); */
            

            medico.img = nombreArchivo;

            medico.save((err, medicoActualizado) => {

                if( err ) {
                    return err;
                }else{

                    return res.status(200).json({
                        ok:true,
                        mensaje: 'Imagen actualizada',
                        medico: medicoActualizado
                    });    
                } 
            });
        });
    }
    if ( tipo === 'hospitales') {
        Hospital.findById(id, (err, hospital) => {

            var pathViejo = './uploads/hospitales/' + hospital.img;
            /* Si existe, elimina la imagen anterior */
            if (fs.existsSync( pathViejo)) {
                fs.unlink( pathViejo );
            }

            hospital.img = nombreArchivo;

            hospital.save((err, hospitalActualizado) => {
                return res.status(200).json({
                    ok:true,
                    mensaje: 'Imagen actualizada',
                    hospital: hospitalActualizado
                });    
            });
        });
    }
}

module.exports = app;