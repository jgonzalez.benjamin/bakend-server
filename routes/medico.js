var express = require('express');

var mdAutenticacion = require('../middlewares/autenticacion');

var app = express();

var Medico = require('../models/medico');

// ================================================================
// 'GET'
// ================================================================
app.get('', (req, res, next) => {

    var desde = req.query.desde || 0;
    desde = Number(desde);

    Medico.find({})
    .skip(desde).populate('usuario', 'nombre img email')
        .populate('hospital','nombre').exec( (err, medicos) => {
            if ( err ) {
                return res.status(500).json({
                    ok: false,
                    mensaje: 'error loading doctors',
                    errors: err
                });
            }
        
            Medico.count({}, (err, conteo)=>{

                if (err) {
                    return res.status(400).json({ 
                        ok:false,
                        errors: err.message
                    });
                }

                res.status(200).json({
                    ok: true,
                    total: conteo,
                    medicos: medicos
                });

            });
        });

});

// ================================================================
// Obtener un solo médico: ID
// ================================================================

app.get('/:id', (req, res) => {
    var id = req.params.id;
    Medico.findById( id ).populate('usuario', 'nombre img email').populate('hospital')
    .exec( (err, medico) => {
        if ( err ) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error not found',
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        if (!medico) {
            return res.status(404).json({
                ok: false,
                mensaje: 'Medico id: '+id+' not found',
                errors: err.message/** gracias al psotman pude hacer esto */
            });   
        }
        res.status(200).json({
            ok: true,
            medico: medico
        });
    });
});

// ================================================================
// 'PUT'
// ================================================================
app.put('/:id', mdAutenticacion.verificaToken, (req, res) => { 
/** poneiendo el ':id' estamos haciendo obligatorio el envio del id en la app */
    var id = req.params.id;

    Medico.findById( id, (err, medico) =>{

        if ( err ) {
            return res.status(500).json({
                ok: false,
                mensaje: 'Error not found',
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        if (!medico) {
            return res.status(404).json({
                ok: false,
                mensaje: 'Medico id: '+id+' not found',
                errors: err.message/** gracias al psotman pude hacer esto */
            });
            
        }

        var body = req.body;

        medico.nombre = body.nombre;
        medico.usuario = req.usuario._id;
        medico.hospital = body.hospital;
        /** por angular yo tengo que mandar el id en esa variables de hospital*/

        medico.save( (err, medicoGuardado) =>{
            if ( err ) {
                return res.status(400).json({
                    ok: false,
                    errors: err.message/** gracias al psotman pude hacer esto */
                });
            }

            res.status(200).json({
                ok: true,
                medico: medicoGuardado
            });

        });

    });


});

// ================================================================
// 'POST'
// ================================================================
app.post('/', mdAutenticacion.verificaToken, (req, res) =>{
    var body = req.body;/** esto solo funciona si y solo si se tiene el body-parser */

    var medico = new Medico({
        nombre: body.nombre,
        img: body.img,
        usuario: req.usuario._id,
        hospital: body.hospital 
    /** por angular yo tengo que mandar el id en esa variable de hospital */
    });

    medico.save( (err, medicoGuardado) =>{
        if ( err ) {
            return res.status(400).json({
                ok: false,
                mensaje: 'Error creating medico',
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        res.status(201).json({
            ok: true,
            medico: medicoGuardado
        });
    });

});

// ================================================================
// 'DELETE'
// ================================================================
app.delete('/:id', mdAutenticacion.verificaToken ,(req, res) =>{
    var id = req.params.id;

    Medico.findByIdAndRemove(id, (err, medicoBorrado) =>{
        if ( err ) {
            return res.status(500).json({
                ok: false,
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        if ( !medicoBorrado ) {
            return res.status(404).json({
                ok: false,
                errors: err.message/** gracias al psotman pude hacer esto */
            });
        }
        res.status(200).json({
            ok: true,
            medico: medicoBorrado
        });
    });
});



module.exports = app;
