var jwt = require('jsonwebtoken');

var SEED = require('../config/config').SEED;

// ================================================================
//  Verificar token   'MIDDLEWERE' esta forma es para blokear las 
//      peticiones que esten debajo
// ================================================================

exports.verificaToken = function(req, res, next) {
    
    /** resibe el toke por la url */
    var token = req.query.token;

    jwt.verify( token, SEED, (err, decoded) => {
        if (err) {
            return res.status(401).json({
                ok:false,
                errors: err.message,
                mensaje: 'Token invalido'
            });
        }

        req.usuario = decoded.usuario;

        next();
    });

}

// ================================================================
//  Verifica ADMIN_ROLE
// ================================================================

exports.verificaADMIN_ROLE = function(req, res, next) {

    var usuario = req.usuario;

    if( usuario.role === 'ADMIN_ROLE' ){
        next();
        return;
    }else {
        return res.status(401).json({
            ok:false,
            errors: err.message,
            mensaje: 'Token invalido'
        });
    }

}

// ================================================================
//  Verifica ADMIN o el mismo usuario
// ================================================================

exports.verificaADMIN_o_MismoUsuario = function(req, res, next) {

    var usuario = req.usuario;
    var id = req.params.id;

    if( usuario.role === 'ADMIN_ROLE' || usuario._id === id ){
        console.log(usuario);
        next();
        return;
    }else {
        return res.status(401).json({
            ok:false,
            errors: err.message,
            mensaje: 'asdf'
        });
    }

}


